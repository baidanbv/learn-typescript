import { LocalGitHubUser } from "types";

export const defaultUser: LocalGitHubUser = {
  login: "baidanbv",
  avatar: "https://avatars.githubusercontent.com/u/51722969?v=4",
  name: "Bohdan Baidan",
  company: '',
  blog: "",
  location: '',
  bio: '',
  twitter: '',
  repos: 4,
  followers: 0,
  following: 0,
  created: "2019-06-11T20:49:41Z",
}
